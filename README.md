# GitLabの使い方

## GitLabの設定

### GitLabのアカウント作成

まず, GitLabでアカウントを作成します.  
このときフルネームを求められますがハンドルネームで構いません.

### GitLabの日本語化

デスクトップなら右上にあるアイコンのようなものをクリックして, Settingsに移動します.  
そして, Prefferd Languageに日本語を設定してください.

## Gitの使い方

### Gitのインストール

「[自分用 Git For Windowsのインストール手順](https://qiita.com/toshi-click/items/dcf3dd48fdc74c91b409)」の通りにしてください.

### Gitの設定

```
git config --global user.name "moshroom"
git config --global user.email "moshroom@fake"
```
のように名前とメールアドレスを設定しましょう.
このとき設定するメールアドレスは他人からも見えるので適当に入力して構いません.

### Gitの使い方

<!--
### 自分用に簡易的に使う方法

- GitLabでリポジトリを作成
- 
-->

#### リポジトリのクローン

`git clone https://gitlab.com/moshroom/how-to-use-gitlab.git` のようにコマンドプロンプトで入力してください.
`https://` の部分はクローンしたいリポジトリの, リポジトリ名のすぐ下にあるプルダウンをSSHからhttpsに変更することで確認できます.

#### リポジトリの編集

ここで力尽きた
